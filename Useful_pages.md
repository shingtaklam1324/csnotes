# Useful Websites for CS

- [Past Papers](https://papacambridge.com/past-papers/cie/sy-qp-ms/a-as-level/computer-science-9608/)
- [ZAK on web Notes](https://http://zakonweb.com/)
- [Topic Support Guide - 1.2.1, 1.2.3](https://www.cambridgeinternational.org/Images/285058-computer-science-topic-guides-9608-.zip)
- [Topic Support Guide - 1.3.1](https://www.cambridgeinternational.org/Images/285021-topic-1.3.1-input-output-and-storage-devices-9608-.pdf)
- [Topic Support Guide - 1.7](https://www.cambridgeinternational.org/Images/285021-topic-1.3.1-input-output-and-storage-devices-9608-.pdf)
- [Published Resources](https://www.cambridgeinternational.org/programmes-and-qualifications/cambridge-international-as-and-a-level-computer-science-9608/published-resources/)
- [Past Papers](https://www.gceguide.xyz/)
- [Past Papers](https://www.gceguide.com/)
