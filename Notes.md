<!--
Shing Tak Lam 2018-2019

CC0 1.0 Universal
-->

# Computer Science Notes

- [Information Representation](#information-representation)
  - [Number Representation](#number-representation)
  - [Images](#images)
  - [Sound](#sound)
  - [Video](#video)
  - [Compression Techniques](#compression-techniques)
- [Communications and Internet Technologies](#communications-and-internet-technologies)
  - [Networks](#networks)
    - [Transmission media](#transmission-media)
    - [Internet and World Wide Web](#internet-and-world-wide-web)
    - [Router, Gateways](#router-gateways)
    - [Client Server Model](#client-server-model)
    - [PSTN, Cellular Networks and Dedicate Lines](#pstn-cellular-networks-and-dedicate-lines)
    - [Bit Streaming](#bit-streaming)
  - [IP Addressing](#ip-addressing)
    - [URL and DNS](#url-and-dns)
  - [Client and Server Side Scripting](#client-and-server-side-scripting)
    - [Example Code Sections](#example-code-sections)
- [Hardware](#hardware)
  - [Input, Output and Storage Devices](#input-output-and-storage-devices)
    - [Sensors](#sensors)
    - [Human Input Devices](#human-input-devices)
      - [Keyboard/Keypad](#keyboardkeypad)
      - [Touch Screen](#touch-screen)
      - [Trackerball Mouse](#trackerball-mouse)
      - [Optical Mouse](#optical-mouse)
    - [Scanners](#scanners)
    - [Printers](#printers)
    - [3D Printer](#3d-printer)
    - [Microphone](#microphone)
    - [Speakers](#speakers)
    - [Secondary Storage](#secondary-storage)
      - [Hard Disc Drive](#hard-disc-drive)
      - [Optical Media](#optical-media)
      - [Solid State Memory](#solid-state-memory)
  - [Main Memory](#main-memory)
  - [Logic Gates and Logic Circuits](#logic-gates-and-logic-circuits)
    - [Logic Gates](#logic-gates)
      - [NOT Gate](#not-gate)
      - [AND Gate](#and-gate)
      - [OR Gate](#or-gate)
      - [XOR Gate](#xor-gate)
      - [NAND Gate](#nand-gate)
      - [NOR Gate](#nor-gate)
- [Processor Fundamentals](#processor-fundamentals)
  - [CPU Architecture](#cpu-architecture)
    - [Stored Program Concept](#stored-program-concept)
    - [CPU Operations](#cpu-operations)
    - [Ports and Peripherals](#ports-and-peripherals)
  - [The Fetch-Execute Cycle](#the-fetch-execute-cycle)
    - [Register Transfer notation](#register-transfer-notation)
  - [The Processor's Instruction Set](#the-processors-instruction-set)
  - [Assembly Language](#assembly-language)
- [System Software](#system-software)
  - [Operating System](#operating-system)
  - [Utility Programs](#utility-programs)
    - [Disc Formatter](#disc-formatter)
    - [Disc Defragmenting Software](#disc-defragmenting-software)
    - [Disc Content Analysis/Repair Software](#disc-content-analysisrepair-software)
    - [Virus Checker / Anti-Virus Software](#virus-checker--anti-virus-software)
    - [File Compression Software](#file-compression-software)
    - [Backup Software](#backup-software)
  - [Library Programs](#library-programs)
  - [Language Translators](#language-translators)
    - [Object Code](#object-code)
    - [Java](#java)
- [Security, Privacy and Data Integrity](#security-privacy-and-data-integrity)
  - [Data Security](#data-security)
  - [System Security](#system-security)
  - [Data Integrity](#data-integrity)
- [Ethics and Ownership](#ethics-and-ownership)
  - [Ethics](#ethics)
  - [Ownership](#ownership)
- [Database and Data Modelling](#database-and-data-modelling)
  - [Database Management Systems](#database-management-systems)
  - [Relational Database Modelling](#relational-database-modelling)
    - [Normalisation](#normalisation)
  - [Data Definition Language and Data Manipulation Language](#data-definition-language-and-data-manipulation-language)
    - [Data Definition Language](#data-definition-language)
    - [Data Manipulation Language](#data-manipulation-language)
    - [Optional Clauses](#optional-clauses)
    - [Relations](#relations)

## Information Representation

### Number Representation

There are 3 main ways that numbers are represented with regard to computers. Binary, Decimal and Hexadecimal.

* Binary = Base 2 => {0, 1}
* Decimal = Base 10 => {0, 1, 2, ..., 9}
* Hexadecimal = Base 16 => {0, 1, 2, ..., 9, A, B, ..., F}

| Decimal | Binary | Hexadecimal |
| ------- | ------ | ----------- |
| 0       | 0000   | 0           |
| 1       | 0001   | 1           |
| 2       | 0010   | 2           |
| 3       | 0011   | 3           |
| 4       | 0100   | 4           |
| 5       | 0101   | 5           |
| 6       | 0110   | 6           |
| 7       | 0111   | 7           |
| 8       | 1000   | 8           |
| 9       | 1001   | 9           |
| 10      | 1010   | A           |
| 11      | 1011   | B           |
| 12      | 1100   | C           |
| 13      | 1101   | D           |
| 14      | 1110   | E           |
| 15      | 1111   | F           |

Each hexadecimal digit corresponds to 4 binary digits (1 nibble).

The amount of different numbers that an n-bit binary number can represent is 2^n, so an 8-bit number can have 256 different values.

To convert between binary and decimal, a table can be used like so:

| 128 | 64  | 32  | 16  | 8   | 4   | 2   | 1   |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 0   | 1   | 1   | 0   | 1   | 0   | 0   | 1   |

In this example, the binary number `0110 1001` corresponds to the decimal number

`0*128 + 1*64 + 1*32 + 0*16 + 1*8 + 0*4 + 0*2 + 1*1 = 105`

Two's complement is used to represent negative numbers in computer systems. To represent a binary number in two's complement form, a similar table can be used:

| -128 | 64  | 32  | 16  | 8   | 4   | 2   | 1   |
| ---- | --- | --- | --- | --- | --- | --- | --- |
| 1    | 1   | 1   | 0   | 1   | 0   | 0   | 1   |

So the two's complement binary number corresponds to the decimal number

`1*(-128) + 1*64 + 1*32 + 0*16 + 1*8 + 0*4 + 0*2 + 1*1 = -23`

Given a number (x), the following steps can be used to find the two's complement form of that number:

* Split the table into two sections. The leftmost bit (-128) is the sign bit (s), and everything else is equal to (m).
* If x < 0, then `s=1`, otherwise `s=0`
* `s * (-128) + m = x`
* rearrange to `m = x - 128s`
* fill in the table

Another way of representing numbers using binary is to use binary coded decimal. In this scheme, each nibble represents 1 decimal digit.

For example,

`1001 0001` would be 91 as `1001` is 9 and `0001` is 1.

This form of BCD is known as packed BCD, and two digits can be encoded into one byte. Unpacked BCD is where each digit is encoded using a whole byte. This has the issue of wasting a lot of storage space, as a maximum of 4 bits is needed to code the decimal digit.

BCD is commonly used when currency is involved, so that rounding errors are minimal and also consistent with the system that is used in decimal.

To represent text in documents, each character must be encoded into a number. The set of symbols or characters that the computer recognises and uses is called a character set. The oldest scheme still in use is the ASCII character set. This encoded the English alphabet, some common symbols as well as numbers into 7 or 8 bits, depending on situation. The characters in each group (numbers, uppercase letters, lowercase letters...) are stored consecutively. The first 32 characters are control characters, which are used to control how text is stored and rendered to the user.

The modern scheme is called Unicode, which has a greater range of characters than ASCII, as it is a superset of ASCII. The first 128 characters in Unicode are the same as ASCII. Unicode has characters for the majority of languages in the world and to represent all of these characters, up to 4 bytes are used per character. Unicode is also an international standard, whereas ASCII is not standardised but just a convention.

### Images

There are 2 main types of image storage system in use. Bitmap (aka Raster) image and Vector images.

Bitmapped images are made up of pixels, which are the smallest image element that can be drawn. The pixels are aligned in rows and columns, and the total number of pixels in an image is called the image resolution, which may also be given as (number of columns) * (number of rows). The screen resolution is a similar concept, where it represents the number of pixels used to show an image on a screen. Colour depth refers to the number of bits used to represent each pixel, and a higher colour depth leads to more colours being availble in the image. The common HTML RGB colour set uses 24 bits to represent colour, leading to over 16.7 million different colours being available.

The file size of bitmapped images can also be approximated by file size = (image resolution) * (colour depth). This is only an approximation as a file header is also part of the image file, and in the header information such as colour depth and profile, width, height, file type, compression and the location of the image within the file.

However, when bitmapped images are scaled up, the image ends up being pixelated. The solution to this problem is to use Vector images, which can be scaled without pixelation as when the image is scaled, the image is recalculated and redrawn. The vector image does not have any pixels, instead it has equations and commands which describe the image using sizes relative to an imaginary canvas and to the other components in the image, therefore as the image is scaled, the rendered version can be easily recalculated. Instead, each vector image contains a drawing list, which contains the commands for drawing each object in the image. Each object has a set of properties, such as colour, line width and gradients.

Vector imaging tends to lead to smaller image file sizes, but it does not contain as much detail when compared to photographs stored in bitmap images. Therefore it is more commonly used for logos and other images that will need to be scaled to both very large and very small sizes, and bitmapped images are used for photography and most general use.

Image editing software will have different capabilities, such as changing colours, applying filters, scaling images and layering images.

### Sound

Sound can be stored in computers using a digital format. The process by which computers convert sound waves to a digital signal is known as sampling. A sensor, such as a [microphone](#Microphone), takes measurements of the amplitude of the wave at a regular frequency, and the analogue signal is converted to digital by means of an Analogue to Digital Converter (ADC). In most audio encoders, the signal is sent through a band limiting filter, which removes high frequency components from the signal before letting the microphone process it.

The number of bits used per sample is known as the sampling resolution. The commonly used sampling resolutions are 8-bit, 16-bit and 24-bit. The higher the sampling resolution, the more accurate the digital signal can represent the initial wave, but the output file size will be larger. For example, CDs use 16-bit sampling resolution as it provides a good compromise between the audio quality and the length of audio stored on the disc.

Other measurements of digital sound files are the sampling rate and the number of channels. The sampling rate is the frequency to taking samples, for example with a sampling rate of 44.1kHz (sampling frequency used for mp3), 44,100 samples are taken each second. The number of channels is the number of different sources of sound. For example, stereo sound has two channels, and 5.1 surround sound has 6 channels.

The size of sound files can be calculated like so: (number of channels) * (sample size) * (sample rate) = (file size)

The quantity (sample size) * (sample rate) can also be referred to as the bit rate of the sound.

One reason for digitising audio is to edit the file digitally. Common functionality include the ability to cut, edit start, end and length, fade and edit the pitch of audio files. Another is to allow compact storage of the sound files, as less physical space is used to store sound files digitally compared to using a traditional media like vinyl.

### Video

Video is encoded as a sequence of images that are shown one by one to the user of the computer. Each image is referred to as a frame, and the number of frames per second is the framerate. Given a high enough framerate, the motion will look continuous, whereas with lower framerates, the image will look like it is jumping.

There are two main ways of encoding video frames, interlaced and progressive.

Progressive encoding is where all of the data from the video frame is stored and presented at the same time, and the rate that pictures are presented to the user is the same as the framerate. Progressive video encoding is used by computer displays, as well as films that have been digitised from the original film.

Interlaced encoding is where the information from a single frame is split into two fields. One field contains the data for the even numbered rows, and the other contains the data for the odd numbered rows. The image is presented by showing one field first then showing the other. This means that there will be data from two frames on the screen at the same time, therefore if there is quick motion in the video, the image will appear blurry at the edges. The rate that pictures are presented to the user is double the frame rate, and appears to the eye as having a higher refresh rate. This is originally used to TV broadcasts and for video recordings.

The main benefit of using Interlaced encoding is the ability to have a higher refresh rate for the same bandwidth, as to achieve the same refresh rate progressive encoded video will need to have double the number of frames. However with advances in network technology and transmission, progressive encoding is being more common as the bandwidth is no longer a limiting factor.

Temporal redundancy is when the pixels in two frames have the same value in the same location. This leads to duplication of information within the video. This can be reduced by using inter-frame compression or temporal compression of the video. Only the changes between two frames are encoded, and any pixels that remain the same are not encoded. This means that in an ideal situation, very few frames (key frames) need to be stored as a whole and the file size can be drastically reduced. However this does not work for video that has a large amount of panning shots or movement in the background, as very few pixels will remain the same. Spatial redundancy is when adjacent pixels in a single video frame have the same value. This can be compressed by using image compression algorithms.

Multimedia container formats are files that contain different components. One example would be the video, audio and subtitles of a film. The usage of these files ensures that the different components will remain synchronised during playback.

### Compression Techniques

There are two main categories of compression techniques, lossy compression and lossless compression. In lossy compression, some of the original data is lost in the process, and the original file cannot be retrieved. In lossless compression, all of the original data is preserved. Therefore no data will be lost in the process.

Lossy compression can typically compress files further, down to around 10% of the original file size. Lossy techniques makes decisions about what is important and what is not, and the less important data is lost in the compression process. One example of this is in sound files, where sounds that the human ear cannot hear are typically removed during compression. Common forms of lossy compression include MP3 and JPEG.

Lossless compression typically can't reduce the file size as much as lossy, only to about 50% of the original size. Lossless techniques typically work by substituting longer sequences with shorter ones. Common examples include Run Length Encoding (RLE) and FLAC.

Run Length Encoding is a form of lossless compression that reduces the size of adjacent, identical symbols (characters, pixels, bytes). The reduced string is encoded into two values, the number of characters in the run (run count), and the symbol in the run (run value). These may be preceded by a control character. For example,

```
aaabbbbccddde

Encoded into 

3a4b2c3d1e
or
a3b4c2d3e1
```

Either form is acceptable, and the trailing `1` in the second form can be omitted in some cases.

## Communications and Internet Technologies

### Networks

#### Transmission media

There are multiple methods to transmit data from one location to another. The two main categories are wired methods and wireless methods

With wired methods, there are 2 main technologies in use. Copper cabling and fibre-optics. Fibre optics is the much more expensive option, but will provide the highest bandwidth as well as the lowest requirement for repeaters. For these reasons, fibre optics is used for the intercontinental underwater cabling and for connections that require sending lots of data over a long distance. Fibre optics use pulses of light to represent 1s and 0s. The cheaper alternative is copper cabling. There are two types, twisted pair and coaxial. Twisted pair is used by existing telephone lines, as well as for LANs and ethernet cabling. Coaxial is used for cable TV. Coaxial has the issue of suffering from interference, whereas twisted pair is affected less by the phenomenon, and Fibre optics least affected by interference.

With wireless media, there are 3 main methods, radio waves, microwaves and satellite. All three transmit information based on electromagnetic radiation and transmit across a spectrum of wavelengths, but each has its own advantages and disadvantages. Satellite can provide internet access almost everywhere across the globe, but has the lowest bandwidth, highest cost and is the most affected by weather. This means that satellite is the most appropriate for temporary set ups or where a permanent wire isn't possible (maritime). One example of radio waves is WiFi, which is used as a common WLAN solution, and provides a good compromise between bandwidth and penetration. Microwaves can have a higher bandwidth than radio waves, however they are much more affected by solid obstacles, and much less likely to penetrate walls, which makes them less attractive to setups which cover a large area or multiple rooms.

In general, wireless media are slower than wired media, but has the benefits of not requiring a wire to be connected to the device for it to be able to access the internet.

#### Internet and World Wide Web

The Internet (which stands for Interconnected Networks), is a massive network of networks and connected computer devices. The TCP/IP Protocol is used to transfer data across the Internet.

The World Wide Web is a collection of interlinked web pages and documents written in Hypertext Markup Language (HTML), stored on web sites, which are typically hosted on web servers. The HTTP Protocol is used to transfer data, which can be accessed by a URL which specifies its location using a browser.

#### Router, Gateways

Routers are used to transfer data from one network to another in the most efficient way possible, by directing the packets of data to the target that it needs to reach.

Gateways are needed when two LANs with different underlying technologies need to interface, and it converts the data packets from one protocol to another.

#### Client Server Model

Servers are devices or software that provides a specific function for the computers using a network. Clients are computers that are connected to the same network which require the function of the servers. Clients do this by making requests on the network, and the servers receive that request, processes it and sends a reply. The resources of the client computer is not shared or used, only the resources of the server.

Common applications of the client-server model include

- Web servers
- Email
- File servers
- Proxy servers
- Print servers

#### PSTN, Cellular Networks and Dedicate Lines

One of the technologies supporting the Internet's operation is the Public Switched Telephone Network, also known as PSTN. Both data and voice and be transferred on PSTN, and through VoIP and other technologies, voice can also be transferred across the Internet.

The main differences are that the internet connection is only in use when sound is being transmitted, whereas on PSTN, the connection is present whether or not any sound is being transmitted. PSTN also has dedicated channels used between the two devices calling each other, and that the connection will remain even during a power outage. With an internet based system, the data can be encrypted and encoded to ensure the data's security and integrity during transfer.

Cellular networks can also be used to support the internet, as information can be sent from one tower to another, as well as from one device to another. The system allows for mobile internet access alongside the voice traffic on the cellular network.

Dedicated lines are used when the bandwidth required is much higher. These are the most expensive, as there is no existing infrastructure to piggy back off of, but performs the best as only internet data packets are sent along it, whereas with the other two, voice and other data are sent on the same network.

#### Bit Streaming

Bit Streaming is the process of continuously transmitting data from a server to a client computer. The process involves the downloaded file to be played back as it is being downloaded. There are two main types of bit streaming, on-demand and real time.

On demand bit streaming is when the user is playing back an video which has happened in the past. The media file has been converted into bit streaming format before it is uploaded to a file server or a content delivery server. The media is then sent to the user's computer, and the file can be played back. As the entire file is already available, the user can pause, rewind and fast forward the playback of the file. As well as that, if the internet access is not continuous, the playback can use a buffer to smooth out and prevent any issues caused by the network connection.

Real time bit streaming is when the event is happening as the file is being played back. The event is captured (by a video camera) and sent to a computer, which then sends the data to all of the devices watching the playback. This means that a buffer cannot be used and the video cannot be paused, rewound or fast forward.

The effect of network speed on bit streaming is that if the network speed is less than the bit rate of the media to be played back, then any real time content will either not play or have serious degrading in quality, and any on demand content will have significant pauses for the buffer to fill up again before playback can resume.

### IP Addressing

Access to the internet requires IP (Internet Protocol) Addressing. The existing standard of IP Addresses is IPv4, which is a 32-bit integer, commonly written as 4 decimal numbers, one for each byte in the IP Address, separated by "`.`", known as dotted decimal notation.

For example
```
00000010 00000000 11111111 00000001
=>
2.0.255.1
```

The original specification for IP Addresses allowed for different classes of IP Addresses, as shown in the table below:

|         | Prefix | Number of bits for netID | Number of bits for hostID |
| ------- | ------ | ------------------------ | ------------------------- |
| Class A | 0      | 7                        | 24                        |
| Class B | 10     | 14                       | 16                        |
| Class C | 110    | 21                       | 8                         |

The netID is used to specify a specific network, and the hostID is used to identify devices on the network. This approach has several problems, first this structure is inflexible, so IP Addresses are going to be wasted if networks have less than 256 devices, as that is the smallest network available. As well as that, there are only 128 different Class A netIDs, which would be used for any networks with more than 65536 devices, which is common in MANs and WANs. The final issue is that there are only about 4 billion different IP Addresses available, and devices connected to the internet has surpassed that number already.

To fix the issue with the inflexibility of class based IP Addressing, Classless Inter-Domain Routing (CIDR) was introduced. This worked by adding a suffix to the 32-bit IP Address, which specified the number of bits used for the netID. This would theoretically double the number of IP Addresses available, but some of the suffixes are not used in practise. The notation is as such:

```
11000011 00001100 00000 110 00001110/00010101
^---------------------- ^----------- ^-------
21bit netID             11bit hostID suffix=(21)
```

A different approach is sub-netting, which takes existing Classed IP Addresses, and further splits up the hostID to allow for less IP Addresses to be wasted.

For example, consider a network like so, where each LAN has less than 60 devices:

```
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ Internet                            ┃
┗┯━━━━━━━━━┯━━━━━━━━━┯━━━━━━━━━┯━━━━━━┛
 │         │         │         │
┏┷━━━━━━┓ ┏┷━━━━━━┓ ┏┷━━━━━━┓ ┏┷━━━━━━┓ 
┃ LAN 1 ┃ ┃ LAN 2 ┃ ┃ LAN 3 ┃ ┃ LAN 4 ┃
┗━━━━━━━┛ ┗━━━━━━━┛ ┗━━━━━━━┛ ┗━━━━━━━┛
```

each LAN would require a Class C IP Address, and over 190 IP Addresses would be wasted for each LAN. An alternative solution is like so:

```
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ Internet                            ┃
┗┯━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
 │
┏┷━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ Router                              ┃
┗┯━━━━━━━━━┯━━━━━━━━━┯━━━━━━━━━┯━━━━━━┛
 │         │         │         │
┏┷━━━━━━┓ ┏┷━━━━━━┓ ┏┷━━━━━━┓ ┏┷━━━━━━┓ 
┃ LAN 1 ┃ ┃ LAN 2 ┃ ┃ LAN 3 ┃ ┃ LAN 4 ┃
┗━━━━━━━┛ ┗━━━━━━━┛ ┗━━━━━━━┛ ┗━━━━━━━┛
```

Only one class C IP Address is needed and very few IP Addresses would be wasted. The implementation of this system would be based on some kind of scheme where the hostID is split. For this example, this can be that the first 2 bits of the hostID specifies the LAN, and the remaining 6 bits the device on that LAN.

The final solution is Network Address Translation (NAT). A network utilising NAT would then look like so:

```
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ Internet                            ┃
┗┯━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
 │
┏┷━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ NAT Box                             ┃
┗┯━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
 │
┏┷━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ Router                              ┃
┗┯━━━━━━━━━┯━━━━━━━━━┯━━━━━━━━━┯━━━━━━┛
 │         │         │         │
 
          Various Devices
```

The NAT box would have one public IP Address, which can be used to both send and receive data. Each device connected to the NAT would be assigned a private IP Address, which can be in one of the following ranges, and these IP Addresses are never public

| Start       | End             |
| ----------- | --------------- |
| 10.0.0.0    | 10.255.255.255  |
| 172.16.0.0  | 172.31.255.255  |
| 192.168.0.0 | 192.168.255.255 |

The public IP Address can be reached across the internet, but private IP addresses can only be accessed from within the LAN, and cannot be accessed across the internet. Consequently, a private IP Address is more secure than a public one. The public IP would be assigned by the ISP, whereas the private IP would be assigned by the router and different networks can use the same private IP Addresses.

#### URL and DNS

The Uniform Resource Locator (URL) is a reference address to a resource on the internet. This is most commonly web pages, but can also be other web services. The format of URLs commonly used for web services look like so:

```
https://en.wikipedia.org/wiki/URL.html
```

| Part             | Name and Purpose                       |
| ---------------- | -------------------------------------- |
| `https://`       | Protocol used to access the server     |
| `en`             | The `en` subdomain of `.wikipedia.org` |
| `.wikipedia`     | The `.wikipedia` subdomain of `.org`   |
| `.org`           | The Top Level Domain (TLD)             |
| `/wiki/URL.html` | The file name                          |

Common Protocols used include `http` (Hypertext Transfer Protocol), `https` (HTTP secure / HTTP over SSL), `mailto` (Email), `ftp` (File Transfer Protocol).

The Domain Name Service is a group of web servers which provide a lookup service to translate between URLs and the IP addresses.

When the user tries to access a web site through its URL, the following steps happen:

1. The Browser passes the URL to the nearest Domain Name Server
2. The DNS searches through its database for the queried URL.
3. If a match is found, the DNS server returns the corresponding IP Address.
    1. If a match is not found, then it forwards the URL to another DNS Server at a higher level.
    2. If this returns an IP address, then the original server adds this URL to its database.
4. The browser then starts a connection with the server using the IP Address and requests the web page
5. The server sends the web page to the browser.
6. The browser displays the web page on the screen.

### Client and Server Side Scripting

Client side scripting is when the program executes on the User's computer (the client). The requested web page would have code, typically JavaScript code embedded in it. When the browser receives the web page, it interprets the JavaScript code. Common uses of client side scripting is to provide user input, as well as animating various parts of the web page and dynamically changing the content on the web page. The server side script executes just before step 6 in the previous section

Server side scripting is when the program is executed on the server before it is sent to the user's computer. Many languages can be used, one of which is PHP. The PHP code is executed between steps 4 and 5 from the previous stage. Common uses of server side scripting is to perform database queries, or other processes which may be computationally heavy and not appropriate to do on the users computer.

#### Example Code Sections

Javascript

```js
function foo() {
    if (condition) {
        // do this
    } else {
        // do that
    }
}

alert("hello world");

var bar = 1;

// Get the value from a DOM element such as an input field.
var mark = document.getElementById("name").value;
```

PHP

```php

function foo() {
    if ($condition)  {
        // do this
    } else {
        // do that
    }
}

echo "hello world";

$bar = 1;
```

## Hardware

### Input, Output and Storage Devices

#### Sensors

Sensors are input devices which allow for the computer system to monitor the environment around it. Typically, sensors produce analogue output which must be processed by an ADC before the computer can process the data. Here are a list of the typical sensors and what each measures

* Thermistor / Temperature Sensor
* Moisture Sensor
* pH Sensor
* Photodiode / Light
* Sound
* Infrared Sensor
* Motion Sensor
* Pressure

#### Human Input Devices

##### Keyboard/Keypad

A keyboard consists of a matrix or array of keys connected in rows and columns, which have a switch underneath each key. When the key is pressed, the switch is turned on. The microprocessor reads the value of each switch in the keyboard, then decodes the keys pressed by comparing it to a ROM, and then outputs the key presses to the computer.

##### Touch Screen

There are 2 main types of touch screens, resistive and capacitive.

Resistive screens consists of 2 charged plates, and pressure on the screen will cause the two plates to touch, completing the circuit. The point of contact is then registered.

Capacitive screens are made from materials that store charge. When the user touches the screen, the charge is transferred to the user's finger. Sensors in the corners detect the charge and registers the point of contact.

With both types, coordinates are used to calculate motion.

Trackpads function similar to touch screens (typically capacitive), and are available on laptops and other devices as an alternative to using a mouse.

##### Trackerball Mouse

In the mouse there is a rubber ball which will roll against the surface that the mice is used on. There are two rollers that are placed perpendicular to the other and touching the ball. As the ball rolls, the rollers will be rotated. Each roller will correspond to one degree of freedom, (X, Y axis). The each roller is connected to a disc with perforations on it. There is a light that is pointing at the disc, and as the disc rotates, the light will be blocked when the hole moves out of position. Behind the disc is a sensor which will detect how much light is getting through. The sensors are then connected to a circuit which processes the data and sends the motion data to the computer.

##### Optical Mouse

Optical mice uses a light source (typically an LED) and sensors (photodiodes, or CCDs like in cameras) to detect movement. The light source shines the light towards the surface that the mouse is placed on, and the sensors take snapshots/readings at regular intervals. The data from the sensors are then fed into the onboard microprocessor to calculate the movement of the mice based on the movement of the image. The movement data is then sent to the computer, which can then use that to move the cursor's position on the screen.

#### Scanners

Scanners can be used to input hard copy documents into computer systems, and the resulting file is stored as an image. The resulting image can then be processed to extract the text inside or do other operations. The way that a scanner works is by using an image sensor similar to ones used in cameras beneath a transparent plate, and the document to be scanned above the glass plate.

#### Printers

There are 2 main types of printers in use, inkjet printers and laser printers.

Inkjet printers function by moving a print head across the piece of paper. The print head consists of nozzles where the ink droplets are fired from onto the paper. The way that this is done is by heating up the ink in the nozzle, vaporising the ink and creating a bubble. When the bubble pops, the ink is then deposited on the page. The vacuum created by the bubble popping then pulls in more ink for the next dot to be printed. The print head is connected by a belt to a stepper motor that controls the print heads motion and moves the head horizontally across the paper.

Laser printers function slightly differently. A laser beam and rotating mirror are used to draw the image onto a photosensitive drum. The drum converts the image into electrostatic charge, which attracts toner. The drum is then rolled against the paper which is charged oppositely. The toner particles stick to the paper and the paper is then discharged to stop it sticking to the drum. The paper is then passed through a fuser, which heats up the paper and keeps the toner in place. Finally, the drum is discharged and the excess toner is collected.

Both types of printers are non-impact (contrasting with dot-matrix printers). Inkjet printers are line printers as they print lines at a time, whereas laser printers print pages at a time. Both can produce high quality images, and inkjets are typically used when the quantity is small, and laser printers are used to produce large numbers of the same document.

#### 3D Printer

3D Printers can be used to create physical objects from a variety of materials. The way that they function is by having a print head, which has a heater and a nozzle. The print head can move horizontally across the print bed, as it is connected to a pair of stepper motors, one for each direction it can move in. Within the print head, the nozzle has a heater, which melts the material it is printing, and squeezes it out of the nozzle. Once the layer has been built up, the print head/bed can be moved vertically using a stepper motor, which allows the printer to build 3D objects one later at a time.

#### Microphone

Microphones work by having a diaphragm, which is a thin piece of material. When sound waves enter the microphone, it causes the diaphragm to vibrate with the sound waves. The diaphragm can be connected to a circuit that will turn the vibrations into electrical signals, either through using a capacitor (condenser microphone), where the vibrations causes changes in the capacitance (and therefore voltage), or with a piezoelectric crystal that turns the vibrations into electrical signals directly. The electrical signal will need to be sampled by an [ADC](#Sound) before the signal can be processed.

#### Speakers

Speakers function as an inverse to microphones. It turns electrical signals into sound waves. It is made up of a diaphragm, a permanent magnet, a coil of wire and the associated hardware to hold everything in place, such as the outer frame and suspension. The way that traditional speakers work is by having a current flow through a wire, causing an electromagnetic field. Changes in the current will cause the size and strength of the magnetic field to fluctuate. If the direction of the current changes, so does the direction of the magnetic field. Depending on the current, the permanent magnet is either attracted or repelled by the electromagnet. This causes the magnet to move, which causes the diaphragm to vibrate. This moves the air infront of the speaker, transmitting the vibration as sound waves. The amount of movement will determine the amplitude of the sound being generated.

#### Secondary Storage

Secondary storage is needed as primary storage is not large enough, as well as the fact that data stored on RAM is volatile, and will be lost when the computer is powered off. Therefore if the data is to be stored for a long period of time it must be stored on secondary storage or backing storage.

##### Hard Disc Drive

Hard Disc drives contain spinning magnetic discs known as platters. There is a read head and a write head, commonly combined into a read-write head, that are used to access the information stored on the platter. The storage space is aligned into concentric circles, each one known as a track. The track is then split radially into sectors, which are the smallest amount of storage space available to the hard disc. Note that by splitting the sectors radially, the ones in the innermost tracks will have the smallest space, and the one in the outer tracks will have more space. Adjacent sectors from different tracks are then combined into blocks. Files would ideally be stored in adjacent blocks, but this is not always the case, and [defragmentation software](#Disc-Defragmentation-Software) is needed to deal with the issue. The way that the read head works is by the fact that a moving magnetic field will generate a current (induction), and by measuring this, it is possible to read the data on the hard disc. Writing onto a hard disc works by using the opposite principle, where an electric current is used to generate a magnetic field that changes the way that the magnetic particles are oriented on the disc.

When the system wants to read from the hard disc, it must first look up the track and sector the data is stored in. The head moves to the correct track and waits until the correct sector to arrive beneath the head. The head then begins reading off the data from the disc and stores it to buffer. This process then continues for each sector that the file is stored in, where the data from the disc is moved to the buffer. When the Hard Disc finishes reading the file, it generates an interrupt.

##### Optical Media

Optical media is also used to store data externally to the computer. It is frequently used as a distributing media as well as a backing storage for computer systems. The main ones in use are Compact Disc (CD), Digital Versatile Disc (DVD) and Blu-Ray Discs. There are several varieties of each, with different properties, however the overall operating principles are the same.

The way that optical media can be used to store data is through the usage of a laser which is shined towards the track. Depending on whether the location the laser reflects off is high (land) or low (pit), it will cause a difference in phase, which can be measured by a sensor (photodiode), and this information can be translated into 1 or 0. For rewritable discs, the principle in use is the same, but the material being used is different, so a high powered laser can be used to heat up and melt the surface, changing the information stored on the disc.

The track on a optical disc is spiral in shape, which means that the head will need to continuously move radially as the disc is being read. Despite this, the disc can still be formatted into sectors, which allows the disc to be used as a direct access device like a hard disc.

##### Solid State Memory

The final type of secondary storage in use is called solid state memory or flash memory. The basis of this is semiconductor technology which allows for information to be stored in the circuitry, with no moving parts necessary. This allows it to be used in locations with vibrations or other conditions where using a Hard Disc or optical media would not be appropriate. As well as that, solid state memory provides higher throughput of data, as there is no latency for physical components to move into locations, so it can be used where data needs to be read/written a lot and quickly. The information is stored in blocks, and like how data on hard disc is stored in sectors, the whole block must be read at one time, and the whole block is erased at one time. This technology can be referred to as a form of [EEPROM](#Main-Memory). The main downside is the limited number of read/write cycles compared to a hard disc, as well as the significantly higher cost for the same amount of storage compared to a hard disc.

### Main Memory

Within the processor, there are a group of [registers](#CPU-Architecture), and external to the processor is cache memory and main memory, which are together called primary storage. The purpose of having main memory is to have quick storage that can be accessed frequently compared to the secondary storage.

The fastest form of primary storage is cache memory, which can be used to store frequently referred to instructions and data, so that it won't need to be retrieved from memory, and this reduces the amount of time needed to execute the instruction. However cache memory ends up being the most expensive, so there is a limited amount used in computer systems.

Random Access Memory (RAM) is used to store the instructions and data currently in use, and is directly connected to the processor. When the user loads up a program, it first goes to RAM before it executes, therefore it is often quicker for a program to launch a second time. RAM is volatile, meaning that when the power to the system is removed, the data in RAM is lost.

There are 2 main types of RAM, static RAM (SRAM) and dynamic RAM (DRAM). DRAM consists of a capacitor to store charge and a transistor to control the current to and from the capacitor. Due to the fact that capacitors slowly leak charge over time, the circuit will need to be refreshed every so oftern, leading to the name ("dynamic"). DRAM stores the bit as a charge, so a high charge is 1 and no charge would be a zero. 

SRAM uses only transistors to build the memory cell to store the data. This ends up being a more complicated circuit. Each bit is stored using a flip flop, which can be toggled to store the data. As the data is stored in the transistors and no charge is leaked in the process, there is no need to refresh the memory.

DRAM is cheaper and offers a higher storage density compared to SRAM, due to the fact that less transistors are used and a less complex circuit is required. However SRAM uses less power, as there is no need to refresh the data in the storage, whereas DRAM must have an external circuit to monitor and refresh regularly. SRAM has better read and write speeds, therefore it is used for cache memory, and the cheaper and larger DRAM is used for the primary storage RAM in the computer.

Read Only Memory (ROM) is another form of primary storage used in computer systems. ROM is non-volatile, and therefore does not lose the information stored in it when power is turned off. As the name implies, the data from ROM can only be read, and cannot be written to. This makes it useful to store the BIOS (Basic Input/Output System, used to start up the computer), bootstrapping or pre-set instructions. There are variants of ROM that can be erased and written to (PROM, EPROM, EEPROM), and flash memory used in solid state discs can be considered a form of EEPROM.

### Logic Gates and Logic Circuits

#### Logic Gates

##### NOT Gate

Outputs the opposite to the input

![NOT-GATE](https://upload.wikimedia.org/wikipedia/commons/b/bc/NOT_ANSI.svg)

| Input | Output |
| ----- | ------ |
| 0     | 1      |
| 1     | 0      |

Formula: `A̅`

##### AND Gate

Outputs when both inputs are on/true/1

![AND-GATE](https://upload.wikimedia.org/wikipedia/commons/6/64/AND_ANSI.svg)

| Input 1 | Input 2 | Output |
| ------- | ------- | ------ |
| 0       | 0       | 0      |
| 0       | 1       | 0      |
| 1       | 0       | 0      |
| 1       | 1       | 1      |

Formula: `A⋅B`

##### OR Gate

Outputs when either input is on

![OR-GATE](https://upload.wikimedia.org/wikipedia/commons/b/b5/OR_ANSI.svg)

| Input 1 | Input 2 | Output |
| ------- | ------- | ------ |
| 0       | 0       | 0      |
| 0       | 1       | 1      |
| 1       | 0       | 1      |
| 1       | 1       | 1      |

Formula: `A+B`

##### XOR Gate

Outputs when one input is on, and not both

![XOR-GATE](https://upload.wikimedia.org/wikipedia/commons/0/01/XOR_ANSI.svg)


| Input 1 | Input 2 | Output |
| ------- | ------- | ------ |
| 0       | 0       | 0      |
| 0       | 1       | 1      |
| 1       | 0       | 1      |
| 1       | 1       | 0      |

Formula:`A⊕B`

##### NAND Gate

Equal to NOT AND

![NAND-GATE](https://upload.wikimedia.org/wikipedia/commons/f/f2/NAND_ANSI.svg)

| Input 1 | Input 2 | Output |
| ------- | ------- | ------ |
| 0       | 0       | 1      |
| 0       | 1       | 1      |
| 1       | 0       | 1      |
| 1       | 1       | 0      |

##### NOR Gate

Equal to NOT OR

![NOR-GATE](https://upload.wikimedia.org/wikipedia/commons/6/6c/NOR_ANSI.svg)

| Input 1 | Input 2 | Output |
| ------- | ------- | ------ |
| 0       | 0       | 1      |
| 0       | 1       | 0      |
| 1       | 0       | 0      |
| 1       | 1       | 0      |

## Processor Fundamentals

### CPU Architecture

#### Stored Program Concept

A stored program computer is one that stores the instructions in electronic memory, compared to other computers which may be designed from one purpose, or computers that require a physical device such as a plug board to store the program instructions. Using the Von Neumann Architecture, the program instructions and the data are stored in the same memory. This means that it is much easier to change the program of the computer and also the computer itself can change the instructions that are to be executed.

#### CPU Operations

The CPU is made up of a group of both special purpose and general purpose registers. The common ones are outlined below

| Register                           | Purpose                                                          |
| ---------------------------------- | ---------------------------------------------------------------- |
| Accumulator (ACC)                  | General Purpose, stores the results of any arithmetic operations |
| Program Counter (PC)               | Stores the address of the next instruction to load               |
| Current Instruction Register (CIR) | Stores the current instruction to be decoded                     |
| Memory Address Register (MAR)      | Stores the memory address to be accessed (read/write)            |
| Memory Data Register (MDR)         | Stores the data to move/ moved from memory                       |
| Index Register (IX)                | Stores the base address to be used in indexed addressing         |

Additionally, there are 4 other components that are used in the processor, the Arithmetic Logic Unit (ALU), which performs the arithmetic and comparison operations used in the processor. There is the system clock, which dictates the frequency and the time taken for each operation, and the Control unit, which controls and coordinates the flow of data and the operations within the CPU, by sending and receiving signals. There is also the status register, which is interpreted as independent bits or flags, and each flag is set by an event, such as overflow, zero or the result of a comparison.

A faster system clock means that more instructions can be executed by the CPU in the same amount of time, and this improves the performance of the system. However, this also increases the power usage, and the heat generated becomes a limiting factor.

```
                 ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
                 ┃ Processor                 ┃
 Address Bus     ┃  ┌─────────────────────┐  ┃ Data Bus
┌────────────────╂──│ Registers           │<─╂──────────┐
│                ┃  └─────────────────────┘  ┃          │
│    Control Bus ┃  ┌─────────────────────┐  ┃          │
│       ┌────────╂──│ System Clock        │  ┃          │
│       │        ┃  └─────────────────────┘  ┃          │
│       │        ┃  ┌─────────┐ +─────────┐  ┃          │
│       │────────╂─>│ Control │─│ ALU     │  ┃          │
│       │        ┃  │ Unit    │ │         │  ┃          │
│       │        ┃  └─────────┘ └─────────┘  ┃          │
│       │        ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━┛          │
│       │                                               │
│       │        ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━┓          │
│       └───────>┃ Main memory               ┃          │
└───────────────>┃                           ┃<─────────┘
                 ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
```

The different buses have different purposes. The control bus controls the actions and timing of all the other components, and also contains the flags. These can be called the control signals. The address bus goes from the MAR to the memory, and sends the memory address to the memory itself. The data bus moves the data around the CPU and also between the memory and the registers.

The buses can also be used to transfer data within the CPU. When that is happening, the address bus carries the location of the register the data is being moved to and the data bus carries the data that is being transferred between registers.

The width of the buses can have different effects. A wider address bus allows for more addresses to be accessed, for example, with a 32-bit address bus, only about 4 billion addresses can be accessed. A wider data bus allows for more bits to be transferred simultaneously, and more data can be transferred at one time, which improves performance as fewer transfers are needed.

#### Ports and Peripherals

Buses and ports are also used to connect peripheral devices to the computer system, for example, the Universal Serial Bus (USB). USB is commonly used as it has become an industry standard, as well as devices automatically detecting and configuring when the attached. USB 3.0 also has support for full duplex transfer of data.  Another reason why USB is commonly used for both devices and charging is because USB can transfer data as well as power.

### The Fetch-Execute Cycle

The Fetch-Execute Cycle is the cycle which the processor functions based off, the cycles goes like so:

* The address stored in the PC is moved to the MAR
* The instruction is copied from the memory address contained in the MAR to the MDR
* The instruction is copied form the MDR to the CIR
* The value in the PC is incremented
* The instruction is decoded
* The instruction is executed

Interrupts are handled by the processor in the following stages after the instruction is executed:

* The processor checks if there is an interrupt
* If there is an interrupt, the Interrupt Service Routine (ISR) is loaded to the PC, the contents in the registers are stored
* The ISR executes
* When the ISR is complete, the contents of the registers are restored

The ISR is a sequence of instructions which describe how to handle a specific interrupt condition. The system will have different ISRs to handle different situations.

#### Register Transfer notation

Register transfer notation is one way to describe the transfer of data within the processor. The Fetch-Execute cycle can be described like so:

```
MAR <- [PC]
PC <- [PC] + 1; MDR <- [[MAR]]
CIR <- [MDR]
```

Line by line, it can be described like so:

```
Move the value from the PC to the ACC.
Increment the PC; Move the value in the address stored in the MAR to the MDR
Move the value from the MDR to the PC
```

The semicolon indicates that two operations are going to occur simultaneously/concurrently.

### The Processor's Instruction Set

There are 5 main modes of addressing used in computer systems, as shown in the table below:

| Mode      | Description                                                                             |
| --------- | --------------------------------------------------------------------------------------- |
| Immediate | The operand is the value to be used                                                     |
| Direct    | The operand is the address where the data to be used is located                         |
| Indirect  | The operand is the address of the address where the data to be used is located          |
| Indexed   | The operand is added to the Index Register (IX)'s value to get the address of the data  |
| Relative  | The operand is the offset from the current address where the value to be used is stored |

The structure of the instructions are like so:

```
OPCODE OPERAND
```

There are 5 main groups of instructions being used within processors, data movement, input and output of data, arithmetic operations, compares and jumps.

Data movement instructions are in 2 main groups. `LD_` or Load instructions move data from the memory to the registers. The way this is done is specified by the target of the move and the addressing mode in use. `STO` or store moves the value from a register (typically ACC) to a memory address (Direct Addressing).

`IN` or input gets a value from the user and stored the value in the ACC. `OUT` or output gets the value from the ACC and writes it to the output, typically as the ASCII character.

`ADD` adds the value stored in a memory address or a register to the value stored in the ALU, and then stores the result of the addition in the ACC. `INC` and `DEC` increments and decrements the value in a register respectively.

`CMP` is used to compare the content of the ACC with either a value or the value stored at a memory address. The result sets the Compare Flag or the Equal flag in ths status register.

`JMP` or jump is an unconditional jump. It moves the operand to the PC. `JP_` are conditional jumps, and performs the jump depending on the value of the compare flag in the status register.

### Assembly Language

Assembly language programs contain many special features which are not translated by an assembler. For example:

* comments
* symbolic names for constants
* labels for addresses
* macros
* subroutines
* directives
* system calls

Macros are pieces of code which can be expanded to simplify more complex programs, and can include anything that is valid assembly code. Subroutines are a sequence of program instructions that perform a specific task and are used multiple times. The main difference is that macros calls are replaced with the definition and subroutine calls are not.

Directives are instructions for the assembler to perform tasks other than assembling instructions. This can be used so that the programmer can specify how a program is assembled, for example, assembling it differently for different platforms.

System calls are ways that the program requires a service provided by the kernel of the operating system.

During the assembling process, comments are removed from the program. In a typical two pass assembler, the assembler first goes through the program once to build a table to convert between symbolic names (constants and labels) and the value or address each corresponds to, as well as the definitions of macros. It does so by first creating an empty table, and whenever a symbol is referenced, it adds it as a key to the table. Whenever the definition of a symbolic name is found, the corresponding address or value is added to the table. In the second pass, the assembler uses the definitions to replace the calls with the corresponding values in the table, and then generates the object code.

[Other types of translators, Questions](#Language-Translators)

## System Software

### Operating System

The operating system is a piece of software that manages computer hardware and software, as well as providing common services for software and the users.

The first task that the OS performs is memory management. The first part of this task is to allocate RAM to programs and processes when they start and to deallocate that RAM when the process ends. The OS also keeps track of which memory locations have been allocated and which are free to use by new processes. As well as that, the OS handles moving data between the main memory (RAM) and the secondary storage (Hard drive). It also handles virtual memory, where some data that is not used as frequently is stored on the secondary storage instead of being stored in RAM. It also provides segmentation, which splits the memory into segments, and allocating each program or process a segment. The program can't access any memory that is not in that segment as it has not been allocated to the program.

The OS also handles security features in computer systems. It sets up username and password for multi-user systems, and performs authentication upon logging in to the system. It implements user access right systems so that what each user has access to can be controlled by an administrator. Additionally, it performs automatic backups and allows for restoring the system to previous backups.

Additionally, the OS also handles hardware management through the installation and management of device drivers of both hardware within the computer system as well as any peripherals that are used with the computer system. It also provides file management systems, through a program or interface for the user to manage files stored on the computer system. The OS handles process management, which involves managing how much CPU (and GPU, Memory...) resource each process is going to receive, so that tasks can occur concurrently. The Operating System provides error detection and recovery mechanisms, as well as [ISRs](#The-Fetch-Execute-Cycle).

The OS will provide some form of user interface (UI), also known as a human computer interface (HCI), of which there are 2 main types, Command Line Interface (CLI) and Graphical User Interface (GUI). The command line is the one that has existed the longest, uses less computer resources and for experienced users can be much quicker to use. It works by requiring the user to enter the command to execute at a prompt. However it requires the user to memorise a large number of program names and acronyms, as well as how each program functions to be able to use the CLI quickly. GUIs tend to be more intuitive for the user, using pictures and text to illustrate what actions need to be done. Therefore it is quicker for most users to use a GUI compared to a CLI. Command Lines do have uses in systems that may not have a screen, or for technicians that are experienced in using it to manage systems and servers.

### Utility Programs

Operating systems often come with utility programs which help users handle the day to day running of a computer system.

Common utility programs included in computer systems include disc formatters, virus checkers, disc defragmenting software, disc content analysis/disc repair software, file compression software and backup software.

The main difference between application software and utility software is that the main purpose of application software is for the benefit of the user, typically performing tasks which would have had to be completed, whereas the main usage of utlity software is for the benefit of the computer system itself.

#### Disc Formatter

Disc formatters are used for formatting discs for a new use. The functions which it performs includes removing the existing data on the disc, setting up a new file system, which is analogous to setting up a table of contents, allowing the operating system to associate a specific memory location with a file, therefore the operating system will be able to read and write on the disc. If necessary, the disc formatter can also partition the drive into separate logical drives.

#### Disc Defragmenting Software

Due to the fact that data on a [Hard Disc Drive](#Hard-Disc-Drive) is stored in sectors, the data for a file should be stored in adjacent sectors to ensure optimal performance. However as the drive is used and files are written to and deleted from the disc, adjacent sectors become less common and files end up being stored on different parts of the disc. This can cause performance issues as the time required oo read each file will be increased. The defragmenter rearranges the files on the disc so that the files are stored in contiguous sectors again. This requires some amount of working space, so if the disc is too full, the defragmenting process cannot happen. The resulting disc should have better performance as it takes less time for the hard drive to read the data on the platters.

#### Disc Content Analysis/Repair Software

The disc content analysis software first checks the drive for any errors, which can be caused by bad sectors on the drive where data cannot be stored. This can be caused by a manufacturing defect or by physical damage to the disc during the usage. If the data from the bad sectors can be read, then the software will try to attempt a repair by moving the data away from the bad sectors onto undamaged sectors. Another situation where disc repair software is necessary is when the OS is in the process of writing to the disc and the power is cut off. There will be partially written data which won't be able to be read, so the disc repair software can either clear that sector, or mark it as a bad sector so it won't be read from or written to.

#### Virus Checker / Anti-Virus Software

A virus checking program is frequently installed as part of a computer system, to protect the system from any viruses that may enter the system. A virus checker would check a file whenever it enters the systems, such as when a foreign drive is mounted on the system, as well as when the user downloads a file. However, this is unable to catch all potential viruses, so it is necessary for the virus checker to be regularly updated as well as all files on the computer checked by it to protect from all potential viruses.

#### File Compression Software

[File Compression](#Compression-Techniques) is frequently used to reduce the file size so that less storage space is used by files and less bandwidth is necessary to transfer files across networks. If the OS does not do this, the user can still implement compression algorithms and handle the compression of files manually.

#### Backup Software

Backup Software is used to automatically perform backups of a computer system. This can ensure that it is more consistent. Common features include scheduling backups to a separate hardware device such as a memory stick, so that it can be performed in the background when no user is using the computer, minimising the performance impact. It would also be able to perform incremental backups, so that only changes in the file cause updates in the backup, leading to less time required to perform the backups.

### Library Programs

Programs being written will commonly make use of library programs, which are collections of existing code written by other authors and are available and distributed across computer systems. The benefits of using libraries include the fact that the code is already written, so that less time will be needed for the programmer to develop the whole program. As well as that, given the library programs are used by many programs and users, it should be thoroughly tested and trusted as being relatively error free. The programmer will also be able to use functions (such as mathematical or graphics libraries) which they may not know how to code it. The commonly used libraries will also conform to industry standards for code quality and performance and would contribute to a more robust program overall.

If the program being used is compiled, then the library will need to be (linked)(#Object-Code) before it can be used. There are two main ways to link libraries, static and dynamic linking. Static linking is where the library's object code is included into the executable file by the linker or loaded before runtime by a loader. This has the benefit of ensuring that the library used is the correct version, not needing the user of the program to have the library installed and allows for optimisation to occur and only load parts of the library that are used by the program.

Dynamic Link Library (DLL) is one implementation of dynamic linking. Dynamic linking is when the program libary is linked at runtime and loaded into memory from a persistent storage. This has the benefit of producing smaller executables, as the library is not included into the executable. DLL files are also only loaded into memory when it is required. As well as that, changes to the DLL will affect all programs automatically, so programs do not need to be recompiled if a DLL is used, which can allow security patches to be applied and distributed much quicker. A single DLL file can also be used for multiple programs while only being loaded into memory once, saving on the amount of memory used by the programs. However it requires the DLL file to be present for the programs to be able to use it, and also changes to the DLL file may break programs which were functional before the update. Finally, malicious changes to DLL files could be used to install a virus or perform other tasks on the user's computer.

### Language Translators

There are 3 main types of language translators, [assemblers](#Assembly-Language), compilers and interpreters. The process of a typical assembler has been outlined above.

A compiler takes a program written in a high-level language and then translates the program into object code. The compiler reads the whole program at once, converts it into an intermediate code, then checks for any errors and if there are any, stops the compiling process and outputs all of the errors found. If there was no errors, the compiler then converts the intermediate code into object code.

Interpreters read each line, analyse it for any errors, and if there is an error, the interpreting process halts. If there was no errors, then the interpreting process continues and the program code is converted into an intermediate code. The intermediate code is then converted into machine code and executed.

The advantages of interpreters are that errors can be identified as they occur, without waiting for the whole program to be analysed. However, errors in sections of code that are not executed will not be discovered until that piece of code is executed, causing an error later. As well as that, the source code of a program must be distributed to all users if an interpreter is to be used, whereas with a compiler, the user has no access to the source code, this is a benefit for copyright and intellectual property reasons, but will require the user of the program to trust that no viruses are present in the program.

For interpreted programs, both the interpreter and the source code must be available for the program to be ran. With compiled programs, only the object code (and a linker), or the executable needs to be present. Compiled code will provide faster execution than is possible with a compiler.

Interpreted programs are typically platform-agnostic, and can be ran on any platform where the interpreter is available as the translated code is platform independent. Compilers and assemblers produce object code, which is platform specific, and can only be ran on specific computers as the translated code is platform dependent.

#### Object Code

Compilers and Assemblers produce object code. Object code is machine code but by itself is not executable. A linker must be used to ensure that any memory locations used for the program and any subroutines or system calls it uses will not interfere. A loader can be used to make sure the memory addresses are consistent and then load the program into memory.

#### Java

Java is a programming language that is partially compiled and partially interpreted. Java is platform independent, meaning the same source code can be used on multiple platforms. A two step translation process is used. First the source code is translated into bytecode by the Java compiler, and this bytecode is stored. When the program is ran, the bytecode is interpreted by the Java Virtual Machine (JVM). This means that the byte code can be executed on any platform where the JVM is available.

## Security, Privacy and Data Integrity

The security of data is whether or not it can be recovered if lost or corrupted. Data integrity is the accuracy and whether the data is up to date. Privacy is that data is only available to authorised users. Data protection laws are present in most countries to control what can and cannot be done with private data.

### Data Security

To prevent data from being lost accidentally, there are several methods to ensure that data is kept secure. One method is to have regular backups to either secondary storage or to a third party in an off site location. The benefit of off site backups is that it won't be affected by any physical damage to the main area that data is being stored at. Disc mirroring or RAID can also be used to protect against hard drive failures by making backups and keeping redundant copies of the same data in storage, so that if some is lost by accident, it can still be recovered. Finally, Uninterruptable Power Supplies (UPS) or backup generators can be used to minimise damage caused by problems to do with electricity.

When making backups, it is important to consider several factors regarding tha backup. The first is the frequency. More frequent backups means that less data would be lost. The storage size is also an issue. Typically it should be several times the size of the file being backed up, to allow multiple copies from different moments in time to backup to, and to allow for disc mirroring strategies. The location of backups is also important, as off site backups would not be damaged by physical damage to the original/building. Incremental backups can also be used, as only files that have been updated need to be backed up, files that have not been changed do not need to be backed up. As well as that, it is typical for the data to be backed up when the data is not being used.

### System Security

To prevent data from being damaged by malicious parties, many strategies can be used. For example, requiring username and password, or biometric measurements can be used to limit access to data. This can be combined with access control, giving different access rights to different users, to minimise the access each user has. The data can also be encrypted, making it useless without the key and more difficult to tamper with. Anti-virus software can also be installed to detect any problems due to malware. Firewalls can be used to stop outside requests from the internet. Physical methods such as locks and CCTV can be used to prevent physical damage to the data. There should also be contingency plans for natural disasters and other events, so that the protocol for what should be done can be established. Digital sigantures of incoming files and messages can also be examined, and this can be used to only allow trusted files to be opened, as well as allowing a backtrace of the source of damage.

The main purpose of system security is to ensure continuity of operation and to ensure the security of the data held within the computer system. The continuity refers to limiting the number of situations where the system needs to be shut down, as well as when the system is shut down for any reason, how long it takes for the system to be back online.

### Data Integrity

The Integrity of the data is whether or not it is correct and up to date, and maintaining this consistently over the lifetime of the data. There are two main error detection measures that can be applied to detect any compromises to the integrity of data, verification and validation.

Validation does not ensure that the data is accurate, only that the data being entered is of the correct type and format. This ensures that the computer will be able to process the data, and also catches some common errors. Common validation checks include:

- Presence Check to ensure that an entry field is not left blank
- Format check to ensure that the data entered is of the correct format, such as a date format
- Length check to ensure that the data is of the correct length (telephone number)
- Range check to ensure that the data is within expected values (month in a date must not exceed 12)
- Type check to ensure that the data entered can be parsed and used by the computer, (no text in a number field)
- Check digit can be calculated to ensure that the data is of the correct format for numeric data (eg. ISBN)


Verification of data means to confirm what has been entered. The common example is requiring the password to be entered twice when a user is requested for a new password. Obviously this does not ensure that the data is accurate, as it is possible for the user to make a mistake in both the initial input of data and the confirmation of data, and as long as the two are consistent, the data has been verified.

One way to ensure the integrity of the data is to use a checksum. A checksum is a calculation based on the data. The checksum would be sent along with the data to the receiving end. The receiving computer would then calculate the checksum of the data it had received. If the two checksums match, then there is no error in transmission of the data.

Another way to ensure the integrity of the data is to use a parity check. The parity of the data is the number of 1s in the data, so for example, 0110 0111 has parity 5. The parity is either even or odd. The parity used is communicated between the two computers.

For example

```
Parity: Odd
         0 111 0011
         ^ ^^^^^^^^
Parity Bit Data
```

the parity bit is 0 as the parity of the data is 5 (odd).

This can be combined into a parity block with multiple bytes of data.

```
Parity Odd:

0 001 1001
1 100 0001
1 011 1000
0 101 1011
1 110 0000
0 101 1011
1 011 1100

1 100 0011 < Parity Byte
```

## Ethics and Ownership

### Ethics

Ethics are a set of moral principles which guide behaviour and decision making, based on different religious and philosophical views. The Association of Computing Machinery (ACM) and the Institute of Electrical and Electronics Engineers (IEEE) published a code of ethics that software engineers should abide by, the [ACM/IEEE Software Engineering Code of Ethics](https://ethics.acm.org/code-of-ethics/software-engineering-code/).

The eight principles shown in the preamble are as such:

1. PUBLIC / Software engineers shall act consistently with the public interest.
2. CLIENT AND EMPLOYER / Software engineers shall act in a manner that is in the best interests of their client and employer consistent with the public interest.
3. PRODUCT / Software engineers shall ensure that their products and related modifications meet the highest professional standards possible.
4. JUDGEMENT / Software engineers shall maintain integrity and independence in their professional judgment.
5. MANAGEMENT / Software engineering managers and leaders shall subscribe to and promote an ethical approach to the management of software development and maintenance.
6. PROFESSION / Software engineers shall advance the integrity and reputation of the profession consistent with the public interest.
7. COLLEAGUES / Software engineers shall be fair to and supportive of their colleagues.
8. SELF / Software engineers shall participate in lifelong learning regarding the practice of their profession and shall promote an ethical approach to the practice of the profession.

Application of the code of ethics should be within context. For example, given a situation where the management declares that testing time for a product is reduced, the primary concern would be clause 3, where the software would not be at the highest standards possible. The secondary concern would be clause 5, where the management has not promoted or managed the project aligned with the other clauses in the code. Clause 6 would also be violated by this, as the programmer would not take responsibility for any errors that could have been detected, as well as not making the management aware of the code of ethics.

One main concept in the code of ethics is the public interest. Within the code of ethics, the public interest can refer to:

* health, safety and wellbeing of the public
* spending of the public money
* security and integrity of public data
* information regarding security breaches
* security of private data, (and no access by security services)

The need for a Code of Conduct in a professional computer systems environment is both for the product that the developers will create, as well as the interactions between developers which should be guided by the code of conduct to remain appropriate.

### Ownership

Copyright is the formal recognition of ownership. If an individual publishes something that has original content, the individual has ownership and can then claim copyright. Copyright can also be claimed by organisations on the work that individuals have done for the organisation. Copyright cannot be applied to ideas nor a component of an already published work.

Copyright can apply to literary work, music, film, radio and TV broadcasts, works of art and computer programs. The justification of copyright is that creation of original work takes time and effort, so if the work can just be copied and reproduced without compensation, it will deter originality and creation. Therefore there is a need for legislation to deter abuses of copyright, but this has the issue of different countries having differing laws with regard to copyright, and which court would have jurisdiction.

Typical copyright legislation will include methods for indicating copyright, how long copyright lasts, whether or not it is transferrable, and a method of registering and applying the copyright legislation.

Before the advent of the internet, breaches of copyright must take place by physically making copies of the information, such as copying vinyls or CDs, or photocopying a piece of text. With the advent of internet, it is much easier to transfer 'pirated' data and copyrighted information across the internet. The reason is that only one digital copy is needed for it to be able to spread, it is very easy to make copies and much harder to track down. Digital Rights Management (DRM) are a colection of techniques that make it more difficult to produce copies and distribute, including encryption of data or the deliberate inclusion of damaged sectors or invalid data. One method which the pirated content is often distributed is by Peer to Peer networking (torrenting), which has led to moves to limit or monitor P2P traffic by the owners of the data, but has opposition as it can be seen as a breach of privacy.

There are 3 main types of software licensing, Open Source, Shareware and Commercial. Before the introduction of each, it is important to not the difference between "free" (price is zero) and "free" (freedom), akin to the difference between "free lunch" and "free speech". This will be disambiguated by italicising the first meaning. All other uses are of the second meaning.

Open Source licensing is where the source code, documents as well as the applications can be used. Typically, the source code is available on the same source and the user can download the source code either with or separately to the application, or only the source code is available and the user has to install it. There are 2 main initiatives within open source software, the Open Source Initiative (OSI), and the Free Software Foundation (FSF).

The general outline of the two initiatives are shown below:

|                                                | OSI | FSF |
| ---------------------------------------------- | --- | --- |
| Freedom to run the program                     | y   | y   |
| Free redistribution                            | y   | y   |
| Source Code                                    | y   | y   |
| Modifications                                  | y   | y   |
| License applies to redistributed copies        | y   | y   |
| Distributing software built from modifications | y   | y   |
| Modifications must be publicly available       | n   | y   |

Shareware are software that are provided for *free*, initially on a trial basis. This can be in two forms, either the *free* version has limited functionality, and the extended functionality can only be used by paying users. The alternative is the full functionality is available for a limited length of time, and after that period of time has expired, the software will no longer be usable, and only after the user has paid will the software be available.

Commercial software is the third form. It is also known as proprietary software, and the source code is not available to the user, and redistributing the software would be in violation of the copyright of the software and can be illegal. This is usually specified in the license that comes with the software, which restricts the number of users or the duration of use. The source code is also not typically available for the user to edit.

Benefits of using commercial software include readily available training and support, typically more robust software, user groups being available to discuss issues. Another benefit is that there is a guarantee of the manufacturer delivering patches, as well as a guaranteed path to upgrading software.

## Database and Data Modelling

### Database Management Systems

A Database Management System (DBMS) provides several features above what a traditional file based approach (spreadsheet). A file based approach causes data repetition and duplication, causes security concerns due to every user having complete access to the whole database, as well as disallowing concurrent access to the file. Changing the format of the database in a file based approach will also cause issues for other applications that use that database file. Contrasting to the file based apporach, the DBMS can build index tables, which allow for searching of the database much quicker than searching through a file. The DBMS also controls the data dictionary about the database, which defines the tables and attributes, as well as how the data is stored physically. The data dictionary can also be used to design validation checks, as the datatype of each attribute is stored in the table. Within the DBMS there is a schema, which describes the relationship between the different tables, even if the actual data in the database is not stored that way. Data modelling is a similar concept, which organises the data and defines how one piece of data relates to another.

The DBMS also has security capabilities, including

* Username and Password Access
* Access Rights and Privileges
* Create regular backups
* Encryption of data
* Definition of different views, which control what each user can see
* Usage monitoring and creating activity logs

DBMSs typically have a developer interface, which allows the Database Administrator (DBA) to access the database, as well as set up views for the other users to use. Another feature of DBMS is a query processor. This works by the user first setting up a query with conditions and criteria, and the DBMS then retrieves all the records that fit those criteria, and displays them. The output from a query can also often be output into a report, which provides a more visually appealing and easier to read format.

High level languages typically have facility for accessing the data in a database through the DBMSs Application Programming Interface (API). One example of this would be PHP, and it's use in [Server Side Scripting](#Client-and-Server-Side-Scripting). This allows programs written in other languages than SQL to be able to access the data and process it, producing the appropriate output, for example in a sales web page, showing the results of the user's search query.

### Relational Database Modelling

The data in a database can be split into several separate tables, data duplication will be reduced. There would be less issues with data integrity and compatibility, as the data is only stored once, so any updates to the data only needs to happen in one place. Accidental deletion of data will cause the DBMS to flag up errors. More complex queries will be simpler and quicker to do, as not every attribute is used in a search. Security will be improved, as it makes it possible to set different access by different users for each table, limiting the access each user has.

In relational databases, each table can also be called an entity, and the fields can be called attributes. Each row is a tuple or a record. The candidate keys are attributes that can be used to uniquely identify a tuple in a database (this can be a combination of multiple attributes or just one attribute, but should be minimal in number). The Primary key is the attribute chosed by the DBA to uniquely identify a tuple in the table. A foreign key is an attribute which refers to the primary key attribute of another table. A relation is a special kind of table with a heading (attribute names) and tuples of the same type, (the attribute types for each tuple are the same). Referential Integrity is the concept that relations between tables built through primary key and foreign key pairs must remain consistent, where the foreign key and the primary key referenced by the foreign key must agree. Secondary Keys are attributes that are not candidate, primary or foreign key fields. Indexing is the process of building up [index tables](#Database-Management-Systems) and using them.

The way that different pieces of data link together can be modelled by an entity-relationship diagram. There are 3 main types of relationship, one to one (1:1), many to one (M:1) or one to many (1:M) and many to many (M:M).

The relationships can be drawn like so:

```
One to One

┌──────────┐            ┌──────────┐
│ Entity 1 │────────────│ Entity 2 │
└──────────┘            └──────────┘

One to Many

┌──────────┐            ┌──────────┐
│ Entity 1 │───────────ᗕ│ Entity 2 │
└──────────┘            └──────────┘

Many to One

┌──────────┐            ┌──────────┐
│ Entity 1 │ᗒ───────────│ Entity 2 │
└──────────┘            └──────────┘

Many to Many

┌──────────┐            ┌──────────┐
│ Entity 1 │ᗒ──────────ᗕ│ Entity 2 │
└──────────┘            └──────────┘
```

1:1 and 1:M relationships are easy to implement. For example, if there is a one to many relationship between A and B, this can be implemented by using the Primary key of A as the Foreign key of B, and linking the two together.

Many to Many relationships can't typically be implemented as is, but it is possible to implement one by inserting a link table. For example, given

```
┌───┐      ┌───┐
│ A │ᗒ────ᗕ│ B │
└───┘      └───┘
```

it is possible to implement it like so:

```
┌───┐      ┌──────┐     ┌───┐
│ A │─────ᗕ│ Link │ᗒ────│ B │
└───┘      └──────┘     └───┘
```

#### Normalisation

|                                                                      | UNF | 1NF | 2NF | 3NF |
| -------------------------------------------------------------------- | --- | --- | --- | --- |
| Primary Key                                                          | y   | y   | y   | y   |
| No repeating records                                                 | y   | y   | y   | y   |
| Cells have one value (atomic)                                        | n   | y   | y   | y   |
| Values depend on the whole of every key (1, no partial dependencies) | n   | n   | y   | y   |
| Values only depend on the keys (2, no transitive dependencies)       | n   | n   | n   | y   |

(UNF=Un-normalised form, 1NF=first normalised form, etc.)

(1):

If a compound key is used, like so:

Cars(**Manufacturer, Model**, ModelFullName, ManufacturerCountry)

`ManufacturerCountry` depends only on the `Manufacturer` Field, and doesn't depend on the `Model` field, therefore the database is not in 2NF.

(2):

For example, if a schema like so is used:

Champions(**Year**, Winner, Team)

The `Team` field does not depend on the `Year` field, only on the non-key `Winner` Field. Therefore the database would not be in 3NF.

### Data Definition Language and Data Manipulation Language

Structured Query Language (SQL) is the programming language provided by a DBMS to handle operations to do with databases. All the creation and modification of tables are done using the DDL, and queries and the maintainence of the data within the database is done using the DML.

#### Data Definition Language

DDL is used to create or alter tables.

The `CREATE` command can be used to create new databases or tables.

```sql
CREATE DATABASE DATABASE_NAME;
CREATE TABLE TABLE_NAME (
    FIELD1 DATATYPE <DESCRIPTIONS>,
    FIELD2 DATATYPE <DESCRIPTIONS>,
    ...
    PRIMARY KEY PK_FIELD_NAME
);
```

Common data types in use include:

| Datatype     | Description                                                           |
| ------------ | --------------------------------------------------------------------- |
| `varchar(X)` | String, `X` is an integer specifying the length, `(X)` can be omitted |
| `Date`       | Date/Time type                                                        |
| `int`        | Integer type                                                          |
| `number(X)`  | Number type, `X` is an integer specifying the length, can be omitted  |

Descriptions of the fields include the `PRIMARY KEY` statement, which would mean that it is not included as it's own row, the `NOT NULL` statement, which requires that there is data in that field.

The `ALTER TABLE` statement can be used to edit tables that have already been created.

To add/change the primary key.

```sql
ALTER TABLE TABLE_NAME
ADD PRIMARY KEY (KEY_NAME);
```

To add a foreign key from another table

```sql
ALTER TABLE TABLE_NAME
ADD FOREIGN KEY (FK_NAME REFERENCES SOURCE_TABLE(PK_NAME));
```

To add a field to a table

```sql
ALTER TABLE TABLE_NAME
ADD FIELD_NAME DATA_TYPE;
```

#### Data Manipulation Language

DML is used when a database is first created, to insert data into it, as well as updating, selecting data using a query and deleting data.

The `INSERT INTO` statement can be used to add new records to the table

```sql
-- Inserting values in the order of the attributes specified in the schema
INSERT INTO TABLE_NAME (VALUE1, VALUE2, ...);

-- Inserting into specific fields of the record
INSERT INTO  TABLE_NAME (FIELD1, FIELD2, ...)
    VALUES (VALUE1, VALUE2, ...);
```

The `UPDATE` statement can be used to change one or more records in a table.

```sql
-- Update every record in the table
UPDATE TABLE_NAME
SET ATTRIBUTE_NAME = VALUE_TO_SET
<CLAUSES>;
```

The `SELECT` statement can be used to make queries in the database.

```sql
-- Select these attributes from every record
SELECT ATTR1, ATTR2, ATTR3, ...
FROM TABLE_NAME
<CLAUSES>;
```

The `DELETE` statement can be used to delete records from a table
```sql
DELETE FROM TABLE_NAME
<CLAUSES>;
```

#### Optional Clauses

Clauses can be added to the end of queries (before the semicolon) to change what data is being operated on

The `WHERE` clause can be used to filter by a condition, and will only return results where the condition is true.

```sql
WHERE CONDITION
```

Valid Conditions include

```sql
FIELD = VAL
FIELD LIKE WILDCARD_STRING -- "*a", "a*", ...
FIELD < VAL
FIELD <= VAL
FIELD > VAL
FIELD >= VAL
FIELD <> VAL
FIELD IN VAL
FIELD BETWEEN VAL1 AND VAL2
```

Multiple conditions can be combined by using `AND` and `OR`

```sql
CONDITION1 AND CONDITION2
CONDITION1 OR CONDITION2
```

The `GROUP BY` clause groups the output data based on a parameter

```sql
GROUP BY FIELD_NAME
```

The `ORDER BY` clause changes the output order based on a parameter

```sql
ORDER BY FIELD_NAME
```

`ASC` and `DESC` can be added to specify the output order of the data, being ascending or descending.

#### Relations

Relational databases can have multiple tables queried at the same time by linking the tables using the foreign/primary key of the tables and the `INNER JOIN` statement.

```sql
TABLE1 INNER JOIN TABLE2 ON TABLE1.KEY_FIELD = TABLE2.KEY_FIELD

-- Example SELECT query
SELECT TABLE1.FIELD1, TABLE1.FIELD2, TABLE2.FIELD3,...
FROM TABLE1
INNER JOIN TABLE2
ON TABLE1.KEY_FIELD = TABLE2.KEY_FIELD
```
